$(document).ready(function() {


var current_form, next_form, previous_form; //Forms
var left, opacity, scale; //Form properties that will animate
var animating; //flag to prevent quick multi-click glitches

document.querySelector('.date-input').onkeyup = function () {
  if (event.keyCode != 8) {  
    if (this.value.length == 2) {
      this.value += '/';
    }
    if (this.value.length == 5) {
      this.value += '/';
    }
  }
};

$(".next-button").click(function(){
	if(animating) return false;
	animating = true;
	
	current_form = $(this).parent();
	next_form = $(this).parent().next();
	
	//activate next step on progressbar 
	$("#progressbar li").eq($("fieldset").index(next_form)).addClass("active");
	
	//show the next form
	next_form.show(); 
	//hide the current_form with style
	current_form.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_form reduces to 0 - stored in "now"
			//1. scale current_form down to 80%
			scale = 1 - (1 - now) * 0.2;
			//2. bring next_form from the right(50%)
			left = (now * 50)+"%";
			//3. increase opacity of next_form to 1 as it moves in
			opacity = 1 - now;
			current_form.css({
        'transform': 'scale('+scale+')',
        'position': 'absolute'
      });
			next_form.css({'left': left, 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_form.hide();
			animating = false;
		}, 
		//comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".previous-button").click(function(){
	if(animating) return false;
	animating = true;
	
	current_form = $(this).parent();
	previous_form = $(this).parent().prev();
	
	//de-activate current step on progressbar
	$("#progressbar li").eq($("fieldset").index(current_form)).removeClass("active");
	
	//show the previous form
	previous_form.show(); 
	//hide the current form with style
	current_form.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_form reduces to 0 - stored in "now"
			//1. scale previous_form from 80% to 100%
			scale = 0.8 + (1 - now) * 0.2;
			//2. take current_form to the right(50%) - from 0%
			left = ((1-now) * 50)+"%";
			//3. increase opacity of previous_form to 1 as it moves in
			opacity = 1 - now;
			current_form.css({'left': left});
			previous_form.css({'transform': 'scale('+scale+')', 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_form.hide();
			animating = false;
		}, 
		//comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".submit-button").click(function(){
	var firstName= document.getElementById("firstName").value;
	var phone= document.getElementById("phone_radio").checked;
	var email= document.getElementById("email_radio").checked;
	var contact="";

	if (firstName.trim() === "") {
		firstName = "Anonymous"
	}

	if (phone){
		contact = ' We will reach you by Phone.'

	}else if(email){
		contact = ' We will reach you by Email.'
	}
	return alert ("Thank You " + firstName + ", we will see you at Game Night!" + contact);
});


});